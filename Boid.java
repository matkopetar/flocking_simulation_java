package flocking_simulation;

public class Boid {
  public double centerX;
  public double centerY;
  public int r2;
  public double vx;
  public double vy;
  public Boolean speedToCenter = true;
  public double orientation = 0;
  public Boolean alert = false;
  public Boolean alert2 = false;
  public double v1x, v1y, v2x, v2y, v3x, v3y, v4x, v4y, v5x, v5y, v6x, v6y, v7x, v7y;
  
  public Boid(int x, int y, int rStart) {
    r2 = rStart;
    centerX = x;
    centerY = y;
  }
  /*
  public void calculateVelocity(double mouseX, double mouseY) {
    double dx = mouseX - centerX;
    double dy = mouseY - centerY;
    double dir = Math.atan2(dy, dx); 
    double speed = Math.sqrt(dx * dx + dy * dy);
    speed = Math.min(speed, 2.0);
    vx = (speed * Math.cos(dir));
    vy = (speed * Math.sin(dir));
  }

  public double calculateDistanceBetweenCenters(double x, double y) {
	  return ((centerX - x) * (centerY - x) + (centerY - y) * (centerY - y));
  }
  
  public double[] getOppositeVelocity(double x, double y) {
	  double dirX = centerX - x;
	  double dirY = centerY - y;
	  double dir = Math.atan2(dirY, dirX);
	  double speed = Math.sqrt(dirX * dirX + dirY * dirY);
	  speed = Math.min(speed, 5.0);
	  double vx = (speed * Math.cos(dir));
	  double vy = (speed * Math.sin(dir));
	  double[] a = {vx, vy};
	  return a;
  } */
}
