package flocking_simulation;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Color;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

public class Main {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI();
            }
        });
    }

    private static void createAndShowGUI() {
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(new DrawBoids());
        f.setSize(1000, 600);
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }

}

class DrawBoids extends JPanel implements MouseListener, MouseMotionListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Point mousePoint;
    public Boid[] boids = new Boid[230];
    public Boid tuna = new Boid(500,100,30);
	public int c = 1001;
	public double[] rnd_goal = {Math.random() * 1450, Math.random() * 750};
	
    public DrawBoids() {

        addMouseListener(this);
        addMouseMotionListener(this);
        for(int i = 0; i < boids.length; i++) {
        	boids[i] = new Boid((int)(Math.random() * 1450), (int)(Math.random() * 750),10);
        }
        
        Timer timer = new Timer(5, new ActionListener() {

			@Override
            public void actionPerformed(ActionEvent e) {
                if (mousePoint != null) {
                	double v1x = 0;
                	double v1y = 0;
                	double v2x = 0;
                	double v2y = 0;
                	double v3x = 0;
                	double v3y = 0;
                	double v4x = 0;
                	double v4y = 0;
                	double v5x = 0;
                	double v5y = 0;
                	double v6x;
                	double v6y;
                	double v7x;
                	double v7y;
                	double v20x = 0;
                	double v20y = 0;

                    for(int i = 0; i < boids.length; i++) {

                    	
                    	v1x = swimTowardsTheCentreOfMass(boids[i])[0];
                    	v1y = swimTowardsTheCentreOfMass(boids[i])[1];
                    	v2x = keepDistanceFromOtherObjects(boids[i])[0];
                    	v2y = keepDistanceFromOtherObjects(boids[i])[1];
                    	v3x = tryToMatchVelocityOfOtherBoids(boids[i])[0];
                    	v3y = tryToMatchVelocityOfOtherBoids(boids[i])[1];
                    	v4x = tendToPlace(boids[i], mousePoint.x, mousePoint.y)[0];
                    	v4y = tendToPlace(boids[i], mousePoint.x, mousePoint.y)[1];
                    	v5x = shuffle(boids[i])[0];
                    	v5y = shuffle(boids[i])[1];
                    	v6x = 0;
                    	v6y = 0;
                    	v7x = 0;
                    	v7y = 0;
                    	Boolean a = false;

                        for(int j = 0; j < boids.length; j++) {
                        	if((boids[j].alert == true || boids[j].alert2 == true) && boids[j] != boids[i] && Math.sqrt((boids[j].centerX - boids[i].centerX) * (boids[j].centerX - boids[i].centerX) + (boids[j].centerY - boids[i].centerY) * (boids[j].centerY - boids[i].centerY)) < 50) {
                        		v6x += boids[j].v7x/25;
                        		v6y += boids[j].v7y/25;
                        		boids[i].alert2 = true;
                        	}
                        	if(boids[j].alert == true) {
                        		a = true;
                        	}
                        }
                        if(rotatePointAroundAnotherPoint(boids[i], tuna.centerX, tuna.centerY, boids[i].orientation)[0] < boids[i].centerX || a == false) {
                			boids[i].alert = false;
                			boids[i].alert2 = false;
                		}
  
                       	double dx = (v1x + v2x + v3x + v4x + v5x + v6x)/5;
                      	double dy = (v1y + v2y + v3y + v4y + v5y + v6y)/5;
                        double angle = Math.atan2(dy, dx);
                        if(angle != boids[i].orientation) {
                        	v7x = rotate(boids[i],(v1x + v2x + v3x + v4x + v5x + v6x)/5, (v1y + v2y + v3y + v4y + v5y + v6y)/5, boids[i].orientation - angle)[0];
                    		v7y = rotate(boids[i],(v1x + v2x + v3x + v4x + v5x + v6x)/5, (v1y + v2y + v3y + v4y + v5y + v6y)/5, boids[i].orientation - angle)[1];
                    		boids[i].v7x = v7x;
                    		boids[i].v7y = v7y;
                    		if(boids[i].orientation > 2*Math.PI) {
                    			boids[i].orientation -= 2*Math.PI;
                    		}
                        }
                        else {
                        	v7x = 0;
                        	v7y = 0;
                        }
                        
                    	boids[i].vx = (v1x + v2x + v3x + v4x + v5x + v6x + v7x)/5;
                    	boids[i].vy = (v1y + v2y + v3y + v4y + v5y + v6y + v7y)/5;
                    	
                    	if(boids[i].alert == true || boids[i].alert2 == true) {
                    		boids[i].centerX += v7x * 1.5;
                    		boids[i].centerY += v7y * 1.5;
                    	}
                    	else {
                    		boids[i].centerX += v7x;
                    		boids[i].centerY += v7y;
                    	}
                    }
                    
                    v20x = chasingFishes(tuna, rnd_goal)[0];
                	v20y = chasingFishes(tuna, rnd_goal)[1];
                    
                    tuna.centerX += v20x;
               		tuna.centerY += v20y;

/*
                    v30x = chasingFishes(tuna2, rnd_goal2)[0];
                	v30y = chasingFishes(tuna2, rnd_goal2)[1];
                    
                    tuna2.centerX += v30x;
               		tuna2.centerY += v30y;
  */             		
                	repaint();

                }
            }
        });
        timer.start();
    }

    protected void paintComponent(Graphics gr) {
        super.paintComponent(gr);
        Graphics2D g = (Graphics2D) gr;
        double cipx;
        double cipy;
        g.setRenderingHint(
                        RenderingHints.KEY_RENDERING,
                        RenderingHints.VALUE_RENDER_QUALITY);

        AffineTransform oldAT = g.getTransform();
        cipx = 0;
        cipy = 0;
        
        for(int i = 0; i < boids.length; i++) {
        	g.setColor(Color.cyan);/*
        	if(boids[i].alert == true) {
        		g.setColor(Color.green);
        	}
        	else if(boids[i].alert2 == true){
        		g.setColor(Color.pink);
        	}*/
            g.translate(boids[i].centerX - cipx, boids[i].centerY - cipy);
        	g.fillOval(-boids[i].r2/2,-boids[i].r2/2, boids[i].r2, boids[i].r2);
        	cipx = boids[i].centerX;
        	cipy = boids[i].centerY;
        }

        g.setColor(Color.red);
        g.translate(tuna.centerX - cipx, tuna.centerY - cipy);
        g.fillOval(-tuna.r2/2,-tuna.r2/2, tuna.r2, tuna.r2);
        cipx = tuna.centerX;
        cipy = tuna.centerY;
        

        g.setTransform(oldAT);

    }
    
    public double[] swimTowardsTheCentreOfMass(Boid boid) {
    	double pcx = 0;
    	double pcy = 0;
    	int br = 0;
    	for(int i = 0; i < boids.length; i++) {
    		double dx = boid.centerX - boids[i].centerX;
    		double dy = boid.centerY - boids[i].centerY;
    		if (boids[i] != boid && Math.sqrt(dx*dx+dy*dy) < 100 ) {
    			pcx += boids[i].centerX;
    			pcy += boids[i].centerY;
    			br++;
    		}
    	}
    	if(br != 0) {
    		pcx = pcx / (br);
    		pcy = pcy / (br);
    	}
    	double[] a = {(pcx - boid.centerX) / 100,(pcy - boid.centerY) / 100};
    	return a;
    }
        /*
    public double[] keepDistanceFromOtherObjects(Boid boid) {
    	double cx = 0;
    	double cy = 0;
    	for(int i = 0; i < boids.length; i++) {
			double dx = boids[i].centerX - boid.centerX;
			double dy = boids[i].centerY - boid.centerY;
    		if (boids[i] != boid && Math.sqrt(dx*dx+dy*dy) < 100) {
    			if(Math.sqrt(dx*dx+dy*dy) < boid.r2 * 1.2) {
    				cx -= 1 / dx;
    				cy -= 1 / dy;
    			}
    		}
    	}
    	double[] a = {cx,cy};
    	return a;
    }
    
    */
    public double[] keepDistanceFromOtherObjects(Boid boid) {
    	double cx = 0;
    	double cy = 0;
    	for(int i = 0; i < boids.length; i++) {
    		if (boids[i] != boid) {
    			double dx = boids[i].centerX - boid.centerX;
    			double dy = boids[i].centerY - boid.centerY;
    			if(Math.sqrt(dx*dx+dy*dy) < boid.r2 * 1.2) {
    				cx -= dx / 5;
    				cy -= dy / 5;
    			}
    		}
    	}
    	double tx = tuna.centerX - boid.centerX;
    	double ty = tuna.centerY - boid.centerY;
    	double dist = Math.sqrt(tx*tx+ty*ty);
    	if( dist < (tuna.r2 + boid.r2) * 6 / 5) {
    		cx -= tx / 3;
    		cy -= ty / 3;
    		boid.alert = true;
    	}
    	else {
    		boid.alert = false;
    	}
/*    	
    	double tx2 = tuna2.centerX - boid.centerX;
    	double ty2 = tuna2.centerY - boid.centerY;
    	double dist2 = Math.sqrt(tx2*tx2+ty2*ty2);
    	if( dist2 < (tuna.r2 + boid.r2) * 6 / 5) {
    		cx -= tx2 / 3;
    		cy -= ty2 / 3;
    		boid.alert = true;
    	}
  */  	double[] a = {cx,cy};
    	return a;
    }

    public double[] tryToMatchVelocityOfOtherBoids(Boid boid) {
    	double px = 0;
    	double py = 0;
    	int b = 0;
    	for(int i = 0; i < boids.length; i++) {
    		double dx = boids[i].centerX - boid.centerX;
    		double dy = boids[i].centerY - boid.centerY;
    		if (boids[i] != boid && Math.sqrt(dx*dx+dy*dy) < 100) {
    			px += boids[i].v7x;
    			py += boids[i].v7y;
    			b++;
    		}
    	}
    	if(b != 0) {
    		px = px / b;
			py = py / b;
    	}
    	double[] a = {(px) / 8,(py) / 8};
    	return a;
    }
    
    public double[] tendToPlace(Boid boid, double x, double y) {
    	double[] a = {(x - boid.centerX) / 100, (y - boid.centerY) / 100};
    	return a;
    }

    public double[] tendToPlaceWithConstantSpeed(Boid boid, double x, double y) {
        double dx = x - boid.centerX;
        double dy = y - boid.centerY;
        double dir = Math.atan2(dy, dx); 
        double speed = Math.sqrt(dx * dx + dy * dy);
        speed = Math.min(speed, 2.0);
        double vx = (speed * Math.cos(dir));
        double vy = (speed * Math.sin(dir));
        double[] a = {vx * 1.3,vy * 1.3};
        return a;
    }

    public double[] shuffle(Boid boid) {
    	double cx = 0;
    	double cy = 0;
    	for(int i = 0; i < boids.length; i++) {
    		cx += boids[i].centerX;
    		cy += boids[i].centerY;
    	}
    	cx /= (boids.length);
    	cy /= (boids.length);
		double dx = cx - boid.centerX;
		double dy = cy - boid.centerY;
    	if (boid.speedToCenter == true) {
            double dir = Math.atan2(dy, dx); 
            double speed = Math.sqrt(dx * dx + dy * dy);
            speed = Math.min(speed, 1);
            Boid nBoid = returnBoidClosestToCenter(boids);
            if(Math.sqrt(dx*dx+dy*dy) == Math.sqrt((nBoid.centerX - cx)*(nBoid.centerX - cx) + (nBoid.centerY - cy) * (nBoid.centerY - cy))) {
            	boid.speedToCenter = false;
            	dir = Math.atan2(-dy,-dx);
            }
            double[] a = {(speed * Math.cos(dir)),(speed * Math.sin(dir))};
    		return a;
    	}
    	else {	
    		dx = -dx;
    		dy = -dy;
    		double dir = Math.atan2(dy, dx); 
    		double speed = Math.sqrt(dx * dx + dy * dy);
    		speed = Math.min(speed, 1);
    		Boid fBoid = returnBoidFurthestToCenter(boids);
    		if(Math.sqrt(dx*dx+dy*dy) == Math.sqrt((fBoid.centerX - cx)*(fBoid.centerX - cx) + (fBoid.centerY - cy) * (fBoid.centerY - cy))) {
    			boid.speedToCenter = true;
            	dir = Math.atan2(-dy,-dx);
    		}
    		double[] a = {(speed * Math.cos(dir)),(speed * Math.sin(dir))};
    		return a;
    	}
    }

    public double[] rotate(Boid boid, double x, double y, double angleDifference) {
        double rx, ry;
        double dx = x + 2.8*Math.cos(boid.orientation);
        double dy = y + 2.8*Math.sin(boid.orientation);
        double newOrientation = Math.atan2(dy, dx);
    	boid.orientation = newOrientation;
        rx = (dx);
        ry = (dy);
        double[] a = {rx,ry};
        return a;
    }
    
    public double[] sardineMove(Boid boid, double[] rnd) {
    	double rx,ry;
		if ((boid.centerX < rnd[0] + 50 && boid.centerX > rnd[0] - 50) && (boid.centerY < rnd[1] + 50 && boid.centerY > rnd[1] - 50)) {
			rnd[0] = randomWithRange(1,1400);
			rnd[1] = randomWithRange(1,750);
	 		rx = tendToPlaceWithConstantSpeed(boid, rnd[0], rnd[1])[0];
	 		ry = tendToPlaceWithConstantSpeed(boid, rnd[0], rnd[1])[1];
	    	double[] a = {rx,ry};
	 		return a;
		}
	 	else {
	 		rx = tendToPlaceWithConstantSpeed(boid, rnd[0], rnd[1])[0];
	 		ry = tendToPlaceWithConstantSpeed(boid, rnd[0], rnd[1])[1];
	    	double[] a = {rx,ry};
	 		return a;
	 	}
		
		
    }
    
    public double[] chasingFishes(Boid tuna, double[] rnd) {
    	double rx = 0;
    	double ry = 0;
    	double cx = 0;
    	double cy = 0;
    	for(int i = 0; i < boids.length; i++) {
    		cx += boids[i].centerX;
    		cy += boids[i].centerY;
    	}
    	cx /= (boids.length);
    	cy /= (boids.length);
    	cx = tuna.centerX - cx;
    	cy = tuna.centerY - cy;
    	if(c > 500) {
    		if (!(Math.sqrt(cx*cx+cy*cy) < 180)) {
    			if ((tuna.centerX < rnd[0] + 50 && tuna.centerX > rnd[0] - 50) && (tuna.centerY < rnd[1] + 50 && tuna.centerY > rnd[1] - 50)) {
    				rnd[0] = randomWithRange(1,1400);
    				rnd[1] = randomWithRange(1,750);
    		 		rx = tendToPlaceWithConstantSpeed(tuna, rnd[0], rnd[1])[0];
    		 		ry = tendToPlaceWithConstantSpeed(tuna, rnd[0], rnd[1])[1];
    		    	double[] a = {rx,ry};
    		 		return a;
    			}
    		 	else {
    		 		rx = tendToPlaceWithConstantSpeed(tuna, rnd[0], rnd[1])[0];
    		 		ry = tendToPlaceWithConstantSpeed(tuna, rnd[0], rnd[1])[1];
    		    	double[] a = {rx,ry};
    		 		return a;
    		 	}
    		}
    		else {
    			rnd[0] = tuna.centerX - 3 * cx;
		 		rnd[1] = tuna.centerY - 3 * cy;
		 		rx = tendToPlace(tuna, rnd[0], rnd[1])[0];
		 		ry = tendToPlace(tuna, rnd[0], rnd[1])[1];
		    	double[] a = {rx,ry};
    			c = 0;
		 		return a;
    		}
    	}
    	else {	
    		c++;
	 		rx = tendToPlace(tuna, rnd[0], rnd[1])[0];
	 		ry = tendToPlace(tuna, rnd[0], rnd[1])[1];
	    	double[] a = {rx*3/2,ry*3/2};
	 		return a;
    	}
    }

    public int randomWithRange(int min, int max)
    {
       int range = (max - min) + 1;     
       return (int)(Math.random() * range) + min;
    }
    
    public double[] rotatePointAroundAnotherPoint(Boid boid, double x, double y, double angle) {
    	double newX = boid.centerX + (x-boid.centerX)*Math.cos(angle) - (y-boid.centerY)*Math.sin(angle);
    	double newY = boid.centerY + (x-boid.centerX)*Math.sin(angle) + (y-boid.centerY)*Math.cos(angle);
    	double[] a = {newX,newY};
    	return a;
    }
																										
    public Boid returnBoidClosestToCenter(Boid[] boids) {
    	double cx = 0;
    	double cy = 0;
    	for(int i = 0; i < boids.length; i++) {
    		cx += boids[i].centerX;
    		cy += boids[i].centerY;
    	}
    	cx /= (boids.length);
    	cy /= (boids.length);
    	Boid min = boids[0];
    	for (int i = 1; i < boids.length; i++) {
    		double dx = boids[i].centerX - cx;
    		double dy = boids[i].centerY - cy;
    		double mindx = min.centerX - cx;
    		double mindy = min.centerY - cy;
    		if(Math.sqrt(dx*dx+dy*dy) < Math.sqrt(mindy*mindy+mindx*mindx)) {
    			min = boids[i];
    		}
    	}
    	return min;
    }
    
    public Boid returnBoidFurthestToCenter(Boid[] boids) {
    	double cx = 0;
    	double cy = 0;
    	for(int i = 0; i < boids.length; i++) {
    		cx += boids[i].centerX;
    		cy += boids[i].centerY;
    	}
    	cx /= (boids.length);
    	cy /= (boids.length);
    	Boid max = boids[0];
    	for (int i = 1; i < boids.length; i++) {
    		double dx = boids[i].centerX - cx;
    		double dy = boids[i].centerY - cy;
    		double maxdx = max.centerX - cx;
    		double maxdy = max.centerY - cy;
    		if(Math.sqrt(dx*dx+dy*dy) > Math.sqrt(maxdx*maxdx+maxdy*maxdy)) {
    			max = boids[i];
    		}
    	}
    	return max;
    }
    
    @Override
    public void mouseMoved(MouseEvent e) {
        mousePoint = e.getPoint();
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
    }

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
